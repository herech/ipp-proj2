

#CSV:xherec00

""" 
" Kódování: UTF-8
" Autor: Jan Herec, xherec00@stud.fit.vutbr.cz
" Datum vytvoření: 11. 4. 2015
" Popis: Třída CSV
"""

import os
import sys
from Const import *
import Parser
import re
import io
import XML

class CSV:
    """ Třída CSV pro načtení CSV souboru a pro provádění operací související s CSV problematikou """
    
    def __init__(self, inputParametersObject):
        """ 
        " Metoda se zavolá po vytvoření objektu. 
        " Zároveň provede vytvoření a inicializaci atributů objektu.
        " @param inputParametersObject objekt, který reprezentuje vstupní parametry
        " @return void
        """
        
        self.__inputParametersObject = inputParametersObject   # uložíme si objekt, který reprezentuje vstupní parametry
        self.__records = []                                    # deklarujeme pole, které bude obsahovat záznamy z CSV souboru
    

    def parseCSVFile(self):
        """ 
        " Metoda parsuje vstupní CSV soubor, tzn. volá syntaktický analyzátor, který zpracuje CSV a načte jeho obsah do pole self.__records
        " @return kód chyby ze třídy Const, určující jestli nastala při parsování CSV souboru chyba
        """
        
        try:
            # otevřeme soubor, nebo standardní vstup
            with io.open(self.__inputParametersObject.getInputFile(), mode='r', encoding='utf-8', newline='') if self.__inputParametersObject.getTypeOfInput() == Const.FILE else sys.stdin as file:

                # načteme obsah CSV souboru do řetězce
                inputCSV=file.read()
                
                # vytvoříme objekt představující syntaktický analyzátor
                parser = Parser.Parser(inputCSV, self.__inputParametersObject.getSeparator(),self.__records)
                
                # začneme parsovat vstupní CSV soubor
                returnCode = parser.beginSyntacticAnalysis()
                
                # pokud syntaktická analýza odhalila chybu ve vstupním CSV souboru, nemá smysl dále pokračovat a propagujeme chybu dále
                if (returnCode != Const.ALL_OK):
                    return returnCode
      
        except IOError:
            return Const.ERROR_FILE_CANNOT_BE_OPENED

        return Const.ALL_OK
        
    def convertCSVToXML(self,xmlObject):
        """ 
        " Metoda konvertuje načtený obsah CSV souboru do formátu XML
        " @param xmlObject pole, které je předáno odkazem a do kterého uložíme jako prvek xml objekt, který vznikne po převodu
        " @return kód chyby ze třídy Const, určující jestli nastala při převodu CSV do XML
        """
        
        namesForColumns = [] # deklarujeme pole, které bude obsahovat názvy pro jednotlivé sloupce        
                
        # určíme jména sloupců v XML
        # pokud je první řádek CSV hlavička
        if (self.__inputParametersObject.isFirstRowHeader() == True):
            header = self.__records[0] # vytáhneme si z pole záznamů hlavičku
                    
            # procházíme indexy položek hlavičky
            for index in range(len(header)):
                # provedeme nahrazení nevalidních znaků pro XML elementy v názvech sloupců
                header[index] = XML.XML.replaceInvalidCharsInElement(header[index], self.__inputParametersObject.getSubstitutionForCSVHeader())
                # ověříme jestli vznikl validní název elementu, pokud ne vrátíme chybu
                if (XML.XML.checkIfNameOfElementIsValid(header[index]) != True):
                    return Const.ERROR_COLUMN_NAME_IN_CSV_CAN_NOT_BE_CONVERTED_TO_XML_TAG
                
            # první záznam - který je hlavičkou odstraníme a uložíme jej do pole, které obsahuje jména sloupců
            namesForColumns = self.__records.pop(0) 
        else:
            # procházíme indexy prvního záznamu a podle nich tvoříme názvy pro elementy, které budou reprezentovat sloupce CSV záznamů
            for index in range(len(self.__records[0])):
                namesForColumns.append(self.__inputParametersObject.getPrefixOfColumnElement() + str(index + 1))
                
        # nastavíme jména řádků v XML
        nameForRows = self.__inputParametersObject.getNameOfLineElement()
        
        # nastavíme příznak atributu index v řádcích XML a poč. hodnotu tohoto atributu
        hasAttrIndex, startValueOfAttrIndex = self.__inputParametersObject.isSetAttrIndex()
        
        # nastavíme příznak, který určuje, jestli se má provádět zotavení z chyb CSV souboru
        doErrorRecovery = self.__inputParametersObject.doErrorRecovery()

        # nastavíme hodnotu, která se má doplňovat do políček, které budou doplňovat v rámci error recovery 
        missingFieldValue = self.__inputParametersObject.getMissingFieldValue()

        # nastavíme příznak, který určuje, jestli sloupce které jsou navíc v nekorektním CSV jsou ignorovány nebo ne
        includeAllColumn = self.__inputParametersObject.includeAllColumn()
        
        maxValueOfColumns = len(namesForColumns) # budeme hledat maximální počet sloupců v záznamu, podle tohoto parametru doplníme sloupce, když je zadán parametr all-columns
        
        # pokud nemáme provádět zotavení z chyb, tak v případě že některý řádek obsahuje jiný počet sloupců než první řádek, vyhodíme chybu
        if (doErrorRecovery == False):
            for row in self.__records:
                if (len(row) != len(namesForColumns)):
                    return Const.ERROR_BAD_NUMBER_OF_COLUMNS_IN_ROW
        
        # pokud máme provádět zotavení z chyb, tak v případě že některý řádek obsahuje jiný počet sloupců než první řádek, 
        # tak chybu nevyhazujeme a řádek případně ořízneme nebo doplníme, nebo ponecháme nezměněný
        else:
            for row_index, row in enumerate(self.__records):
                # pokud řádek obsahuje méně sloupců než první řádek, doplníme buňky
                if (len(row) < len(namesForColumns)):
                    for col_index, col in enumerate(namesForColumns):
                        if (col_index >= len(row)):
                            self.__records[row_index].append(missingFieldValue)
                # pokud řádek obsahuje více sloupců než první řádek a není zadán parametr all-columns ořízneme sloupce navíc
                elif (len(row) > len(namesForColumns) and includeAllColumn == False):
                    self.__records[row_index] = row[:len(namesForColumns)]
                # pokud řádek obsahuje více sloupců než první řádek a je zadán parametr all-columns neděláme nic
                elif (len(row) > len(namesForColumns) and includeAllColumn == True):
                    # průběžně si ukládáme max počet sloupců, který se kdy na nějakém řádku vyskytl
                    maxValueOfColumns = len(row) if (len(row) > maxValueOfColumns) else len(namesForColumns)
                    self.__records[row_index] = row[:]  
                        
        # konvertujeme všechny problematické znaky v obsahových buňkách CSV
        for row_index, row in enumerate(self.__records):
            for col_index, col in enumerate(self.__records[row_index]):
                row[col_index] = XML.XML.convertProblematicCharsInString(col)
        
        # pokud nemáme ignorovat sloupce které jsou v záznamu navíc, tak pro ně vytvoříme název xml značky, která je bude obalovat
        if (includeAllColumn == True):
            for index in range(maxValueOfColumns):
                if (index >= len(namesForColumns)):
                    namesForColumns.append(self.__inputParametersObject.getPrefixOfColumnElement() + str(index + 1))
        
        # do výstupního parametru uložíme instanci třídy XML
        xmlObject.append(XML.XML(self.__records, namesForColumns,nameForRows,hasAttrIndex,startValueOfAttrIndex, 
                        self.__inputParametersObject.getRootElement(), self.__inputParametersObject.generateXMLHeader()))
        
        return Const.ALL_OK
    
    @classmethod    
    def checkIfSeparatorIsValid(cls, separator):
        """ 
        " Metoda zkontroluje správnost separátoru buněk CSV souboru
        " @param separator separátor, který se bude kontrolovat
        " @return True nebo False v závislosti na tom, jestli je název xml elementu validní
        """
        
        # pokud je separátor jakýkoliv tisknutelný znak, nebo identifikátor TAB, tak je validní, jinak není validní
        if (separator == "TAB" or (len(separator) == 1 and ord(separator) > 31 and ord(separator) != 127)):
            return True
        else:
            return False
    
    @classmethod    
    def checkIfCharInFieldIsValid(cls, char, fieldSeparator):
        """ 
        " Metoda zkontroluje správnost znaku - jestli se může vyskytovat uvnitř CSV buňky
        " @param char znak, který se bude kontrolovat
        " @fieldSeparator oddělovač buněk, který se nesmí vyskytnout v buňce, pokud není uzavřená do uvozovek
        " @return True nebo False v závislosti na tom, jestli je znak validní
        """
        
        # pokud se jedná o EOF, vrátíme false
        if (char == Const.TOKEN_EOF):
            return False
        
        # pokud je znak nepovolený jako obsah neescapované buňky, vrátíme false
        if (ord(char) < ord("\u0020")
        or ord(char) == ord("\u0022")
        or char == fieldSeparator
        or ord(char) == ord("\u007F")):
            
            return False
        else:
            return True
