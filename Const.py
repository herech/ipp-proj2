

#CSV:xherec00

""" 
" Kódování: UTF-8
" Autor: Jan Herec, xherec00@stud.fit.vutbr.cz
" Datum vytvoření: 10. 4. 2015
" Popis: Třída "konstant", které se budou používat globálně
"""

class Const:
    """ Třída definuje globálně konstanty """
    
    # konstanty definující akci
    PRINT_HELP = 0
    CONVERT_CSV_TO_XML = 1

    # konstanty definující typ vstupu, výstupu
    STDOUT = 0;
    STDIN = 1
    FILE = 3;

    # konstanty definující chybové kódy
    ALL_OK  = 0
    ERROR_BAD_FORMAT_OF_PARAMS = 1
    ERROR_FILE_CANNOT_BE_OPENED = 2
    ERROR_FILE_CANNOT_BE_WRITTEN = 3
    ERROR_BAD_INPUT_FILE_FORMAT = 4
    ERROR_ENTERED_INVALID_XML_TAG = 30
    ERROR_COLUMN_NAME_IN_CSV_CAN_NOT_BE_CONVERTED_TO_XML_TAG = 31
    ERROR_BAD_NUMBER_OF_COLUMNS_IN_ROW = 32
    
    # tokeny
    TOKEN_DEFAULT = 0
    TOKEN_EOF = 1
    TOKEN_CRLF = 2
    TOKEN_SEPARATOR = 3
    TOKEN_FIELD = 4
    TOKEN_ERROR = -1

    # text nápovědy
    HELP_MESSAGE = "*************************** NÁPOVĚDA ***************************" + "\n" \
        "*** OBECNÉ INFORMACE ***" + "\n" + "\n" \
        "Skript získává vstupu dokument ve formátu CSV, tento převádí do formátu XML. " \
        "Způsob převodu lze ovlivnit skrze zadané parametry." + "\n" + "\n" \
        \
        "*** PARAMETRY SKRIPTU ***" + "\n" + "\n" \
        "--help - vypsání nápovědy, nelze kombinovat s jinými parametry" + "\n" + "\n" \
        \
        "--input=filename - zadaný vstupní CSV soubor v UTF-8. " \
        "Pokud nebude parametr zadán načte se vstupní CSV soubor ze standardního vstupu." + "\n" + "\n" \
        \
        "--output=filename - textový výstupní XML soubor s obsahem převedeným ze vstupního souboru. " \
        "Pokud nebude parametr zadán zapíše se obsahem převedený ze vstupního souboru na standardní výstup." + "\n" + "\n" \
        \
        "-n - negenerovat XML hlavičku na výstup skriptu" + "\n" + "\n" \
        \
        "-r=root-element - jméno párového kořenového elementu obalující výsledek. " \
        "Pokud nebude zadán, tak se výsledky neobalují kořenovým elementem, ač to porušuje validitu XML" + "\n" + "\n" \
        \
        "-s=separator - nastavení separátoru (jeden znak) buněk (resp. sloupců) na každém řádku vstupního CSV, " \
        "kromě tisknutelných znaků jako například mezera, středník či čárka je podporován i identifikátor TAB pro tabulátor jako oddělovač; "\
        "implicitní hodnota je znak čárka (,)" + "\n" + "\n" \
        \
        "-h=subst - první řádek CSV souboru slouží jako hlavička podle níž se odvodí jména elementů XML. " \
        "Každý nepovolený znak je nahrazen řetězcem subst. Je-li -h zadáno pouze jako volba, je subst implicitně pouze znak pomlčka (-). " \
        "První záznam CSV se potom již neobjevuje jako obsah ve výsledném XML. " \
        "V případě, že tento parametr/volba zcela chybí, budou jména elementů pro buňky (resp. sloupce) generovány dle parametru -c=column-element." + "\n" + "\n" \
        \
        "-c=column-element - určuje prefix jména elementu column-elementX, který bude obalovat nepojmenované buňky (resp. sloupce), " \
        "kde X je inkrementální čitač od 1. Implicitní hodnotou column-element je řetězec col." + "\n" + "\n" \
        \
        "-l=line-element - jméno elementu, který obaluje zvlášť každý řádek vstupního CSV; implicitní hodnota je row." + "\n" + "\n" \
        \
        "-i - zajistí vložení atributu index s číselnou hodnotou do elementu line-element" \
        "(tento parametr se musí kombinovat s parametrem -l; jinak nastane chyba kombinace parametrů)." + "\n" + "\n" \
        \
        "--start=n - inicializace inkrementálního čitače pro parametr -i na zadané kladné celé číslo n včetně nuly (implicitně n = 1). " \
        "Není-li tento parametr kombinován s -i a -l, nastane chyba parametrů." + "\n" + "\n" \
        \
        "-e, --error-recovery - zotavení z chybného počtu sloupců na neprvním řádku" \
        "(první řádek bude sloužit pro odvození správného počtu sloupců) tj. každý chybějící sloupec bude doplněn" \
        "prázdným polem, přebývající sloupce budou ignorovány. Pokud nebyl zadán tento parametr" \
        "a vstup obsahuje na některém řádku špatný počet sloupců, tak je skript ukončen s chybou." + "\n" + "\n" \
        \
        "--missing-field=val - Parametr je povolen pouze v kombinaci s --error-recovery (resp. -e). " \
        "Pokud nějaká vstupní buňka (sloupec) chybí, tak je doplněna zde uvedená hodnota val místo pouze prázdného pole. " \
        "Problematické znaky jsou konvertovány na odpovídající zápisy v XML pomocí metaznaků." + "\n" + "\n" \
        \
        "--all-columns - Parametr je povolen pouze v kombinaci s --error-recovery (resp. -e). " \
        "Sloupce, které jsou v nekorektním CSV navíc, nejsou ignorovány, ale jsou také vloženy do výsledného XML. " \
        "Pokud je tento parametr navíc v kombinaci s -h, tak sloupce, ke kterým nebyla prvním řádkem definována hlavička, " \
        "budeme dle parametru -c=column-element značit column-elementX, kde X je pořadí sloupce na daném řádku" \
        "(např. třetí sloupec, který byl na daném řádku první bez určené hlavičky, bude označen column-element3)." + "\n"
