

#CSV:xherec00

""" 
" Kódování: UTF-8
" Autor: Jan Herec, xherec00@stud.fit.vutbr.cz
" Datum vytvoření: 11. 4. 2015
" Popis: Třída XML
"""

import os
import sys
from Const import *
import re

class XML:
    """ Třída pro XML výstup načteného CSV souboru a pro provádění operací související s XML problematikou """
    
    # třídní proměnná, která představuje vzor pro první znak XML názvu značky
    _nameStartCharPattern = ":|[A-Z]|_|[a-z]|[\u00C0-\u00D6]|[\u00D8-\u00F6]|[\u00F8-\u02FF]|[\u0370-\u037D]" \
                             "|[\u037F-\u1FFF]|[\u200C-\u200D]|[\u2070-\u218F]|[\u2C00-\u2FEF]|[\u3001-\uD7FF]" \
                             "|[\uF900-\uFDCF]|[\uFDF0-\uFFFD]"
    
    # třídní proměnná, která představuje vzor pro neprvní znak XML názvu značky
    _nameCharPattern = _nameStartCharPattern + "|-|\\.|[0-9]|\u00B7|[\u0300-\u036F]|[\u203F-\u2040]"
    
    # třídní proměnná, která představuje vzor pro XML název značky
    _namePattern = "^" + "(" + _nameStartCharPattern + ")" + "(" + _nameCharPattern + ")*" + "$"
    
    # vzor, kterého nesmí nabývat název xml značky
    _nameBadPattern = "^(X|x)(M|m)(L|l).*$"
    
    def __init__(self, records, namesForColumns,nameForRows,hasAttrIndex,startValueOfAttrIndex, rootElement, generateXMLHeader):
        """ 
        " Metoda se zavolá po vytvoření objektu. 
        " Zároveň provede vytvoření a inicializaci atributů objektu.
        " @param records záznamy CSV
        " @param namesForColumns jména sloupců
        " @param nameForRows jména řádků
        " @param hasAttrIndex příznak, jestli se bude ve výsledném XML vkládat do elementů řádků atribut index s číselnou hodnotou
        " @param startValueOfAttrIndex inicializace čítače pro atribut index
        " @param rootElement jméno párového kořenového XML elementu
        " @param generateXMLHeader příznak, jestli se má generovat XML hlavička 
        " @return void
        """
        
        self.__records = records                              # záznamy CSV
        self.__namesForColumns = namesForColumns              # jména sloupců
        self.__nameForRows = nameForRows                      # jména řádků
        self.__hasAttrIndex = hasAttrIndex                    # příznak, jestli se bude ve výsledném XML vkládat do elementů řádků atribut index s číselnou hodnotou
        self.__startValueOfAttrIndex = startValueOfAttrIndex  # inicializace čítače pro atribut index
        self.__rootElement = rootElement                      # jméno párového kořenového XML elementu
        self.__generateXMLHeader = generateXMLHeader          # příznak, jestli se má generovat XML hlavička 
        
    def getXMLToOutput(self):
        """ 
        " Metoda vrátí XML na výstup (soubor, stdout)
        " @return void
        """
        
        outputXML = ""
        outputXML += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" if (self.__generateXMLHeader) else ""
        outputXML += "<" + self.__rootElement + ">\n" if (self.__rootElement != "") else ""
        gap =  "    " if (self.__rootElement != "") else ""
        
        indexValue = self.__startValueOfAttrIndex
        for row in self.__records:
            attrIndex = "" if (self.__hasAttrIndex == False) else " index=\"" + str(indexValue) + "\""
            outputXML += gap + "<"+ self.__nameForRows + attrIndex + ">\n"
            indexValue += 1
            for col_index, col in enumerate(row):
                outputXML += gap + "    <"+ self.__namesForColumns[col_index] + ">\n"
                outputXML += gap + "        " + col + "\n"
                outputXML += gap + "    </"+ self.__namesForColumns[col_index] + ">\n"
            outputXML += gap + "</"+ self.__nameForRows + ">\n"
                
        outputXML += "</" + self.__rootElement + ">\n" if (self.__rootElement != "") else ""
        
        return outputXML
    
    @classmethod    
    def checkIfNameOfElementIsValid(cls, nameOfElement):
        """ 
        " Metoda zkontroluje správnost názvu xml elementu
        " @param nameOfElement jméno xml elementu, který se bude kontrolovat
        " @return True nebo False v závislosti na tom, jestli je název xml elementu validní
        """

        return re.search(cls._namePattern, nameOfElement) != None and re.search(cls._nameBadPattern, nameOfElement, re.DOTALL) == None
        
    @classmethod    
    def replaceInvalidCharsInElement(cls, nameOfElement, replacement):
        """ 
        " Metoda nahradí nevalidní znaky v názvu xml elementu řetezcem, který je předán jako argument a takto nvě vzniklý název vrací
        " @param nameOfElement jméno xml elementu, ve kterém se budou nahrazovat nevalidní znaky
        " @param replacement řetězec, kterém se budou nahrazovat nevalidní znaky
        " @return nově vzniklý název elementu
        """

        newNameOfElement = "" # nový název elementu, vzniklý po nahrazení nevalidní ch znaků
        firstChar = True      # příznak, že se jedná o první znak názvu elementu
        
        # procházíme název po znacích
        for char in nameOfElement:
            
            # pokud se jedná o první znak názvu elementu
            if (firstChar == True):
                firstChar = False
                
                # pokud je znak nevalidní, provedeme náhradu za řetězec replacement
                if (re.search(cls._nameStartCharPattern, char) == None):
                    newNameOfElement += replacement
                
                # pokud je znak validní nebudeme jej ničím nahrazovat
                else:
                    newNameOfElement += char
                    
            # pokud se jedná o neprvní znak názvu elementu
            else:
                # pokud je znak nevalidní, provedeme náhradu za řetězec replacement
                if (re.search(cls._nameCharPattern, char) == None):
                    newNameOfElement += replacement
                
                # pokud je znak validní nebudeme jej ničím nahrazovat
                else:
                    newNameOfElement += char
                
        return newNameOfElement
            

    @classmethod    
    def convertProblematicCharsInString(cls, string):
        """ 
        " Metoda konvertuje problematické znaky v řetězci na odpovídající XML entity
        " @param string řětězec ibsahující problematické znaky
        " @return nový řětezec s konvertovanými problematickými znaky
        """

        newString = ""
        for char in string:
            if (char == "\""):
                newString += "&quot;"
            elif (char == "'"):
                newString += "&apos;"
            elif (char == "&"):
                newString += "&amp;"
            elif (char == ">"):
                newString += "&gt;"
            elif (char == "<"):
                newString += "&lt;"
            else:
                newString += char
        
        return newString
