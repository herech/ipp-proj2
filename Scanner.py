

#CSV:xherec00

""" 
" Kódování: UTF-8
" Autor: Jan Herec, xherec00@stud.fit.vutbr.cz
" Datum vytvoření: 12. 4. 2015
" Popis: Třída Scanner
"""

from Const import *
import CSV
import sys

class Scanner:
    """ Třída reprezentuje lexikální analyzátor pro analýzu CSV souboru """
    
    STATE_DEFAULT = 0
    STATE_EOF = 1
    STATE_CR = 2
    STATE_CRLF = 3
    STATE_SEPARATOR = 4
    STATE_FIELD = 5
    STATE_CR = 6
    STATE_CRLF = 7
    STATE_FIELD_ESCAPED = 8
    STATE_FIELD_NONESCAPED = 9
    
    def __init__(self, inputCSV, fieldSeparator):
        """ 
        " Metoda se zavolá po vytvoření objektu. Zároveň provede vytvoření a inicializaci atributů objektu.
        " @param inputCSV řetězec, který obsahuje načtený CSV soubor
        " @param fieldSeparator oddělovač buněk v CSV souboru
        " @return void
        """
        
        self.__inputCSV = inputCSV                         # načteme řetězec reprezentující vstupní CSV
        self.__positionInCSV = 0                           # pozice v CSV souboru, na které lexikální analyzátor se nachází
        self.__tokenType = Const.TOKEN_DEFAULT             # typ aktuálně načteného tokenu
        self.__fieldSeparator = fieldSeparator             # nastavíme oddělovač buněk v CSV souboru
        self.__currentState = Scanner.STATE_DEFAULT        # stav lexikálního analyzátoru
        self.__tokenBuffer = []                            # buffer, do kterého se ukládají tokeny, které jsou načteny v syntaktickém analyzátoru přes epsilon pravidla
                                                           # - z bufferu pak přednostně načítá funkce getToken(), čímž se eliminují některé problémy při syntaktické analýze
        self.__tokenValue = ""                             # hodnota tokenu, který jsme načetli ze vstupu a který vrátíme syntaktickému analyzátoru
        self.__lengthOfInputCSV = len(inputCSV)            # počet znaků řetězece reprezentující vstupní CSV
        
        # slovník, kde indexy představují koncové stavy (KA tak bude při ověřování jestli může přijmout nějaký řetězec) 
        self.__setOfFinalStates = {
                                    Scanner.STATE_EOF : 1,
                                    Scanner.STATE_CRLF : 1,
                                    Scanner.STATE_SEPARATOR : 1,
                                    Scanner.STATE_FIELD : 1
                                  }                       
    
    
    def getNextToken(self):
        """ 
        " Metoda vrací následující token, je implementována pomocí KA
        " @return vrací list, kde první prvek je typ tokenu a druhý prvek je hodnota tokenu, v případě chyby nastaví obě položky na False
        """
        
        # pokud je fronta tokenů neprázdná, tak namísto čtení tokenu vrátíme první token z fronty
        if (len(self.__tokenBuffer) > 0):
        
            tokenFromBuffer = self.__tokenBuffer.pop(0)
            return [tokenFromBuffer[0], tokenFromBuffer[1]]
        

        self.__currentState = Scanner.STATE_DEFAULT    # provedeme reset stavu KA
        self.__tokenValue = ""                         # provedeme reset hodnoty načteného tokenu
        self.__tokenType = Const.TOKEN_DEFAULT         # provedeme reset typu načteného tokenu
        
        # budeme vykonávat smyčku, dokud z ní násilně nevyskočíme, to se může stát, pokd přečteme "znak" EOF, 
        # nebo se dostaneme do koncového stavu, nebo pro daný načtený symbol nemůžeme udělat přechod
        
        while True:
        
            currChar = self.__readNextChar() # načteme další znak
            
            # implementace KA, kde na základě současného stavu a vstupu určíme výstup a následující stav
            if (self.__currentState == Scanner.STATE_DEFAULT):
                    
                if (currChar == "\""):
                    self.__currentState = Scanner.STATE_FIELD_ESCAPED
                    
                elif (currChar == self.__fieldSeparator):
                    self.__currentState = Scanner.STATE_SEPARATOR
                    self.__tokenType = Const.TOKEN_SEPARATOR
                    self.__tokenValue += currChar
                    break # načetli jsme token a vyskočíme až za konstrukci while
                    
                elif (currChar == "\r"):
                    self.__currentState = Scanner.STATE_CR
                    
                elif (CSV.CSV.checkIfCharInFieldIsValid(currChar,self.__fieldSeparator)):
                    self.__currentState = Scanner.STATE_FIELD_NONESCAPED
                    self.__tokenValue += currChar
                
                elif (currChar == Const.TOKEN_EOF):
                    self.__currentState = Scanner.STATE_EOF
                    self.__tokenType = Const.TOKEN_EOF
                    self.__tokenValue += "EOF"
                    break # načetli jsme token a vyskočíme až za konstrukci while
                    
                else:
                    break # nepodporovaný vstupní symbol, ukončíme automat s chybou
                    
            elif (self.__currentState == Scanner.STATE_CR):
                
                if (currChar == "\n"):
                    self.__currentState = Scanner.STATE_CRLF
                    self.__tokenType = Const.TOKEN_CRLF
                    self.__tokenValue += "CRLF"
                    break # načetli jsme token a vyskočíme až za konstrukci while
                
                else:
                    break # nepodporovaný vstupní symbol, ukončíme automat s chybou
                    
            elif (self.__currentState == Scanner.STATE_FIELD_NONESCAPED):
                
                if (CSV.CSV.checkIfCharInFieldIsValid(currChar,self.__fieldSeparator)):
                    self.__currentState = Scanner.STATE_FIELD_NONESCAPED
                    self.__tokenValue += currChar
                
                else:
                    self.__currentState = Scanner.STATE_FIELD
                    self.__tokenType = Const.TOKEN_FIELD
                    self.__ungetChar() # přečetli jsme znak, který už není hodnotou buňky CSV
                    break # načetli jsme token a vyskočíme až za konstrukci while
                    
                    
            elif (self.__currentState == Scanner.STATE_FIELD_ESCAPED):
                
                if (CSV.CSV.checkIfCharInFieldIsValid(currChar,self.__fieldSeparator)):
                    self.__currentState = Scanner.STATE_FIELD_ESCAPED
                    self.__tokenValue += currChar
                
                elif (currChar == "\""):
                    nxtChar = self.__readNextChar()
                    if (nxtChar == "\""):
                        self.__currentState = Scanner.STATE_FIELD_ESCAPED
                        self.__tokenValue += currChar
                    
                    else:
                        self.__currentState = Scanner.STATE_FIELD
                        self.__tokenType = Const.TOKEN_FIELD
                        self.__ungetChar() # přečetli jsme znak, který už není hodnotou buňky CSV
                        break # načetli jsme token a vyskočíme až za konstrukci while
                    
                elif (currChar == self.__fieldSeparator):
                    self.__currentState = Scanner.STATE_FIELD_ESCAPED
                    self.__tokenValue += currChar
                    
                elif (currChar == "\r"):
                    self.__currentState = Scanner.STATE_FIELD_ESCAPED
                    self.__tokenValue += currChar
                    
                elif (currChar == "\n"):
                    self.__currentState = Scanner.STATE_FIELD_ESCAPED
                    self.__tokenValue += currChar
                
                else:
                    break # nepodporovaný vstupní symbol, ukončíme automat s chybou
        
        # END WHILE
        
        # pokud aktuální stav automatu je stavem koncovým, je vše v pořádku, vrátíme typ tokenu a jeho hodnotu
        if (self.__currentState in self.__setOfFinalStates):
            return [self.__tokenType, self.__tokenValue]
        
        # pokud došlo k lexikální chybě (nacházíme se v nekoncovém stavu) vrátíme chybu
        else:
            return [Const.TOKEN_ERROR, "TOKEN_ERROR"]
    
    def __readNextChar(self):
        """
        " Metoda vrátí následující znak z řetězcové reprezentace vstupního CSV
        " @return Vrací znak z pole self.__inputCSV na indexu self.__positionInCSV, který je zároveň inkrementován. 
        "         Případně EOF, pokud již byly všechny znaky z pole přečtěny.
        """
    
        # pokud se ukazatel do pole pohybuje v rámci mezí pole
        if (self.__positionInCSV < self.__lengthOfInputCSV):
            nextChar = self.__inputCSV[self.__positionInCSV]
            self.__positionInCSV += 1
            return nextChar
        
        # pokud ukazatel ukazuje mimo pole, už nejsou na vstupu žádné další znaky a vracíme EOF
        else:
            self.__positionInCSV += 1
            return Const.TOKEN_EOF;
        
    
    def __ungetChar(self):
        """
        " Metoda dekrementuje hodnotu atributu positionInCSV, čímž simuluje chování funkce známé z jazyka C ungetchar
        " @return void
        """
        
        if (self.__positionInCSV > 0):
            self.__positionInCSV -= 1
        
    
    def ungetToken(self, token):
        """
        " Metoda ukládá do bufferu přečtený token, tak aby se mohl přečít při dalším volání funkce getToken znovu
        " @return void
        """
        
        self.__tokenBuffer.append(token)
        
    