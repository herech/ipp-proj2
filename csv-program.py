

#CSV:xherec00

""" 
" Kódování: UTF-8
" Autor: Jan Herec, xherec00@stud.fit.vutbr.cz
" Datum vytvoření: 10. 4. 2015
" Popis: Skript získává na vstupu dokument ve formátu CSV, tento převádí do formátu XML. 
"        Způsob převodu lze ovlivnit skrze zadané parametry.
""" 

import sys
from Const import *
from InputParameters import *
from CSV import *

# vytvoříme instanci třídy pro zpracování a práci s parametry
inputParameters = InputParameters(sys.argv)

# parsujeme parametry
retCode = inputParameters.checkAndGetParameters()

# pokud se při parsování vyskytla chyba ukončíme s touto chybou program
if (retCode != Const.ALL_OK):
    
    # pokud je příčnou chyby nesprávný formát parametrů
    if (retCode == Const.ERROR_BAD_FORMAT_OF_PARAMS):
        print("ERROR: BAD FORMAT OF PARAMS", file=sys.stderr)
    
    # pokud je příčinou chyby zadaný název XML značky, jež není podle W3C validní
    elif (retCode == Const.ERROR_ENTERED_INVALID_XML_TAG):
        print("ERROR: ENTERED INVALID XML TAG", file=sys.stderr)
    
    sys.exit(retCode)

# pokud chce uživatel vytisknout nápovědu, tak ji vytiskneme a ukončíme program
if (inputParameters.getTypeOfAction() == Const.PRINT_HELP):
    print(Const.HELP_MESSAGE)
    sys.exit(Const.ALL_OK)
    
# vytvoříme objekt pro zpracování CVS souboru a předáme mu instanci objektu reprezentující vstupní parametry
csv = CSV(inputParameters)

# parsujeme vstupní CSV soubor
retCode = csv.parseCSVFile()

# pokud se při parsování CSV vyskytla chyba ukončíme s touto chybou program
if (retCode != Const.ALL_OK):
    
    # pokud je příčnou chyby nesprávný formát vstupního souboru
    if (retCode == Const.ERROR_BAD_INPUT_FILE_FORMAT):
        print("ERROR: ERROR BAD INPUT FILE FORMAT", file=sys.stderr)
    
    # pokud je příčinou chyby to, že soubor se nepodařilo otevřít pro čtení
    elif (retCode == Const.ERROR_FILE_CANNOT_BE_OPENED):
        print("ERROR: ERROR FILE CANNOT BE OPENED", file=sys.stderr)
    
    sys.exit(retCode)
    
# konvertujeme obsah načteného CSV souboru do XML
xmlObject = []
retCode = csv.convertCSVToXML(xmlObject)

# pokud se při převodu CSV do XML vyskytla chyba ukončíme s touto chybou program
if (retCode != Const.ALL_OK):
    
    # pokud je příčinou chyby to, že hlavička obsahovala sloupec který byl nevalidní pro název xml elementu
    if(retCode == Const.ERROR_COLUMN_NAME_IN_CSV_CAN_NOT_BE_CONVERTED_TO_XML_TAG):
        print("ERROR: ERROR COLUMN NAME IN CSV CAN NOT BE CONVERTED TO XML TAG", file=sys.stderr)
    
    # pokud je příčinou chyby to, že některý řádek obsahuje nesprávný počet sloupců
    elif (retCode == Const.ERROR_BAD_NUMBER_OF_COLUMNS_IN_ROW):
        print("ERROR: ERROR BAD NUMBER OF COLUMNS IN ROW", file=sys.stderr)
    
    sys.exit(retCode)
    
# získáme výstupní XML
xmlObject = xmlObject.pop()
xmlToOutput = xmlObject.getXMLToOutput()

# pokud máme zapisovat na stdout
if (inputParameters.getTypeOfOutput() == Const.STDOUT):
    print(xmlToOutput,end="")
    
# pokud máme zapisovat do souboru
else:
    try:
        # zapíšeme do souboru
        with io.open(inputParameters.getOutputFile(), mode='w', encoding='utf-8', newline='') as file:
            file.write(xmlToOutput)
      
    except IOError:
        sys.exit(Const.ERROR_FILE_CANNOT_BE_WRITTEN)

sys.exit(Const.ALL_OK)


