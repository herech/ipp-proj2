

#CSV:xherec00

""" 
" Kódování: UTF-8
" Autor: Jan Herec, xherec00@stud.fit.vutbr.cz
" Datum vytvoření: 10. 4. 2015
" Popis: Třída InputParameters
"""

import os
import sys
from Const import *
import XML
from CSV import *

class InputParameters:
    """ Třída zpracovává vstupní parametry skriptu """
    
    def __init__(self, argv):
        """ 
        " Metoda se zavolá po vytvoření objektu. 
        " Zároveň provede vytvoření a inicializaci atributů objektu.
        " @param argv argumenty předané programu 
        " @return void
        """
        self._argv = argv                              # parametry předané programu
        self._typeOfInput = Const.STDIN;               # typ vstupu skriptu: standardní vstup nebo výstup ze souboru
        self._inputFile = ""                           # jméno vstupního souboru (pokud jde vstup ze souboru)
        self._typeOfOutput = Const.STDOUT;             # typ výstupu skriptu: standardní výstup nebo výstup do souboru
        self._outputFile = ""                          # jméno výstupního souboru (pokud jde výstup do souboru)
        self._typeOfAction = Const.CONVERT_CSV_TO_XML  # typ akce, kterou skript provede: vypsání nápovědy a ukončení nebo převod CSV do XML 
        self._generateXMLHeader = True                 # příznak, jestli se má generovat XML hlavička 
        self._rootElement = ""                         # jméno párového kořenového XML elementu
        self._separator = ","                          # oddělovač sloupců v CSV souboru
        self._firstRowInCSVIsHeader = False            # příznak, jestli je první řádek v souboru CSV hlavička
        self._substitutionForCSVHeader = "-"           # substitutuce nevalidních znaků v hlavičce CSV
        self._columnElement = "col"                    # název pro nepojmenované sloupce
        self._lineElement = "row"                      # název pro XML elementy obalující řádky CSV 
        self._setAttrIndex = False                     # příznak, jestli se bude ve výsledném XML vkládat do elementů řádků atribut index s číselnou hodnotou
        self._startCounterForIndex = 1                 # inicializace čítače pro atribut index
        self._errorRecovery = False                    # příznak, jestli se má provádět zotavení z chyb ve formátu CSV
        self._missingFieldValue = ""                   # hodnota, která se doplní do prázdných buněk CSV
        self._includeAllColumns = False                # sloupce, které jsou v nekorektním CSV navíc nejsou ignorovány a jsou zahrnuty do výsledku
        
    
    def checkAndGetParameters(self):
        """ 
        " Metoda zkontroluje a načte parametry a jejich hodnoty, jež byly předány programu.
        " @return Kód chyby ze třídy Const, určující jestli nastala při zpracování parametrů chyba
        """
        
        # proměnné, které určují kolikrát se daný parametr vyskytl v předaných argumentech (pokud se vyskytl více než jedenkrát, nastane chyba)
        numberOfParamR = 0
        numberOfParamN = 0
        numberOfParamS = 0
        numberOfParamH = 0
        numberOfParamC = 0
        numberOfParamL = 0
        numberOfParamI = 0
        numberOfParamStart = 0
        numberOfParamE = 0
        numberOfParamMissingField = 0
        numberOfParamAllColumns = 0
        numberOfParamHelp = 0
        numberOfParamInput = 0
        numberOfParamOutput = 0
        
        # příznak, že se již nějaký parametr vyskytl (hlídáme tak možnost zadání --help jako jediného parametru)
        someParamWasGiven = False
        
        argv = self._argv  # zkopírujeme seznam argumentů do proměnné
        del argv[0]        # odstraníme ze seznamu 1. prvek (název programu - ten nás nezajímá)
        
        # procházíme argumenty a zpracováváme je jako parametry
        for param in argv:
            
            # pokud se jedná o parametr help, který je prvním parametrem
            if (param == "--help" and numberOfParamHelp == 0 and someParamWasGiven == False):
                self._typeOfAction = Const.PRINT_HELP
                numberOfParamHelp += 1
            
            # pokud se jedná o parametr input, který se zatím nevyskytl a nevyskytl se ani parametr help
            elif ("--input=" in param and numberOfParamInput == 0 and numberOfParamHelp == 0):
                
                # načteme hodnotu parametru, pokud žádnou nemá, vrátíme chybu
                paramVal = param.split("=")
                paramVal = paramVal[1]
                if (paramVal == ""):
                    return Const.ERROR_BAD_FORMAT_OF_PARAMS

                # nastavíme jako typ vstupu soubor a také jeho jméno
                self._typeOfInput = Const.FILE
                self._inputFile = paramVal
                
                numberOfParamInput += 1
            
            # pokud se jedná o parametr output, který se zatím nevyskytl a nevyskytl se ani parametr help
            elif ("--output=" in param and numberOfParamOutput == 0 and numberOfParamHelp == 0):
                
                # načteme hodnotu parametru, pokud žádnou nemá, vrátíme chybu
                paramVal = param.split("=")
                paramVal = paramVal[1]
                if (paramVal == ""):
                    return Const.ERROR_BAD_FORMAT_OF_PARAMS
                
                # nastavíme jako typ výstupu soubor a také jeho jméno
                self._typeOfOutput = Const.FILE
                self._outputFile = paramVal
                
                numberOfParamOutput += 1
            
            # pokud se jedná o parametr n, který se zatím nevyskytl a nevyskytl se ani parametr help
            elif (param == "-n" and numberOfParamN == 0 and numberOfParamHelp == 0):
                
                self._generateXMLHeader = False; # nebudeme generovat XML hlavičku
                
                numberOfParamN += 1
            
            # pokud se jedná o parametr r, který se zatím nevyskytl a nevyskytl se ani parametr help
            elif ("-r=" in param and numberOfParamR == 0 and numberOfParamHelp == 0):
                
                # načteme hodnotu parametru, pokud žádnou nemá, vrátíme chybu
                paramVal = param.split("=")
                paramVal = paramVal[1]
                if (paramVal == ""):
                    return Const.ERROR_BAD_FORMAT_OF_PARAMS
                
                # zkontrolujeme správnost názvu XML elementu, případně vrátíme chybu
                if (XML.XML.checkIfNameOfElementIsValid(paramVal) == False):
                    return Const.ERROR_ENTERED_INVALID_XML_TAG
                
                # nastavíme jméno párového kořenového elementu v XML výstupu
                self._rootElement = paramVal
                
                numberOfParamR += 1
            
            # pokud se jedná o parametr s, který se zatím nevyskytl a nevyskytl se ani parametr help
            elif ("-s=" in param and numberOfParamS == 0 and numberOfParamHelp == 0):
                
                # načteme hodnotu parametru, pokud žádnou nemá, vrátíme chybu
                paramVal = param.split("=")
                paramVal = paramVal[1]
                if (paramVal == ""):
                    return Const.ERROR_BAD_FORMAT_OF_PARAMS
                
                # zkontrolujeme správnost separátoru, případně vrátíme chybu
                if (CSV.checkIfSeparatorIsValid(paramVal) == False):
                    return Const.ERROR_BAD_FORMAT_OF_PARAMS
                
                # nastavíme separátor sloupců CSV
                self._separator = "\t" if (paramVal == "TAB") else paramVal
                numberOfParamS += 1
            
            # pokud se jedná o parametr h, který se zatím nevyskytl a nevyskytl se ani parametr help
            elif ((param == "-h" or "-h=" in param) and numberOfParamH == 0 and numberOfParamHelp == 0):
                
                self._firstRowInCSVIsHeader = True # příznak, že první řádek v souboru CSV je hlavička
                
                # pokud je zadána substituce pro nevalidní XML znaky
                if ("-h=" in param):
                    
                    # načteme hodnotu parametru, pokud žádnou nemá, vrátíme chybu
                    paramVal = param.split("=")
                    paramVal = paramVal[1]
                    if (paramVal == ""):
                        return Const.ERROR_BAD_FORMAT_OF_PARAMS
                
                    self._substitutionForCSVHeader = paramVal # řetězec pro nahrazení nevalidních znaků v hlavičce CSV
               
                numberOfParamH += 1
            
            # pokud se jedná o parametr c, který se zatím nevyskytl a nevyskytl se ani parametr help
            elif ("-c=" in param and numberOfParamC == 0 and numberOfParamHelp == 0):
                
                # načteme hodnotu parametru, pokud žádnou nemá, vrátíme chybu
                paramVal = param.split("=")
                paramVal = paramVal[1]
                if (paramVal == ""):
                    return Const.ERROR_BAD_FORMAT_OF_PARAMS
                
                # zkontrolujeme správnost názvu XML elementu, případně vrátíme chybu
                if (XML.XML.checkIfNameOfElementIsValid(paramVal) == False):
                    return Const.ERROR_ENTERED_INVALID_XML_TAG
                
                # nastavíme název pro nepojmenované sloupce
                self._columnElement = paramVal

                numberOfParamC += 1
            
            # pokud se jedná o parametr l, který se zatím nevyskytl a nevyskytl se ani parametr help
            elif ("-l=" in param and numberOfParamL == 0 and numberOfParamHelp == 0):
                
                # načteme hodnotu parametru, pokud žádnou nemá, vrátíme chybu
                paramVal = param.split("=")
                paramVal = paramVal[1]
                if (paramVal == ""):
                    return Const.ERROR_BAD_FORMAT_OF_PARAMS
                
                # zkontrolujeme správnost názvu XML elementu, případně vrátíme chybu
                if (XML.XML.checkIfNameOfElementIsValid(paramVal) == False):
                    return Const.ERROR_ENTERED_INVALID_XML_TAG
                
                # nastavíme název pro řádky CSV ve výstupním XML
                self._lineElement = paramVal

                numberOfParamL += 1
            
            # pokud se jedná o parametr i, který se zatím nevyskytl a nevyskytl se ani parametr help
            elif (param == "-i" and numberOfParamI == 0 and numberOfParamHelp == 0):
                self._setAttrIndex = True  # nastavíme příznak, že budeme vkládat atribut index do elementů řádků
                numberOfParamI += 1
            
            # pokud se jedná o parametr start, který se zatím nevyskytl a nevyskytl se ani parametr help
            elif ("--start=" in param and numberOfParamStart == 0 and numberOfParamHelp == 0):
                
                # načteme hodnotu parametru, pokud žádnou nemá, vrátíme chybu
                paramVal = param.split("=")
                paramVal = paramVal[1]
                if (paramVal == ""):
                    return Const.ERROR_BAD_FORMAT_OF_PARAMS
                
                # pokusíme se uložit hodnotu jako číslo, přitom může nastat chyba
                try: 
                    self._startCounterForIndex = int(paramVal)
                    if (self._startCounterForIndex < 0):
                        raise ValueError("záporné číslo")
                except ValueError:
                    return Const.ERROR_BAD_FORMAT_OF_PARAMS

                numberOfParamStart += 1
            
            # pokud se jedná o parametr e, který se zatím nevyskytl a nevyskytl se ani parametr help
            elif ((param == "-e" or param == "--error-recovery") and numberOfParamE == 0 and numberOfParamHelp == 0):
                self._errorRecovery = True  # nastavíme příznak, že budeme provádět zotavení z chyb v souboru CSV
                numberOfParamE += 1
            
            # pokud se jedná o parametr missing-field, který se zatím nevyskytl a nevyskytl se ani parametr help
            elif ("--missing-field=" in param and numberOfParamMissingField == 0 and numberOfParamHelp == 0):
                
                # načteme hodnotu parametru, pokud žádnou nemá, vrátíme chybu
                paramVal = param.split("=")
                paramVal = paramVal[1]
                if (paramVal == ""):
                    return Const.ERROR_BAD_FORMAT_OF_PARAMS
                
                self._missingFieldValue = paramVal
                
                numberOfParamMissingField += 1
            
            elif (param == "--all-columns" and numberOfParamAllColumns == 0 and numberOfParamHelp == 0):
                self._includeAllColumns = True  # zahrneme do výsledků i sloupce které jsou v nekorektním CSV navíc
                numberOfParamAllColumns += 1
            
            # pokud formát zadaných parametrů neodpovídá předepsanému formátu, vracíme chybu
            else:
                return Const.ERROR_BAD_FORMAT_OF_PARAMS 
                
            someParamWasGiven = True # nastavíme příznak, že byl zadán nějaký parametr, který se uplatní v následující iteraci
        
        
        # ošetření závislostí a vazeb mezi jednotlivými argumenty
        
        # pokud je zadán parametr i, tak musí být zadán -i parametr -l
        if (numberOfParamI > 0):
            if (numberOfParamL == 0):
                return Const.ERROR_BAD_FORMAT_OF_PARAMS
             
        # pokud je zadán parametr --start, tak musí být zadán i parametr -i a parametr -l
        if (numberOfParamStart > 0):
            if (numberOfParamI == 0 or numberOfParamL == 0):
                return Const.ERROR_BAD_FORMAT_OF_PARAMS
        
        # pokud jsou parametry --missing-fields nebo --all-columns, tak musí být zadán i parametr -e 
        if (numberOfParamMissingField > 0 or numberOfParamAllColumns > 0):
            if (numberOfParamE == 0):
                return Const.ERROR_BAD_FORMAT_OF_PARAMS        
                
        return Const.ALL_OK  # parametry jsou v pořádku
        
    def getTypeOfAction(self):
        """ 
        " Metoda vrátí typ akce, který se má provést.
        " @return typ akce, který se má provést, viz constants.py
        """
        return self._typeOfAction
    
    def getTypeOfInput(self):
        """ 
        " Metoda vrátí typ vstupu
        " @return typ vstupu: stdout nebo jméno souboru, viz constants.py
        """
        return self._typeOfInput
        
    def getInputFile(self):
        """ 
        " Metoda vrátí jméno vstupního souboru.
        " @return jméno vstupního souboru.
        """
        return self._inputFile
    
    def getTypeOfOutput(self):
        """ 
        " Metoda vrátí typ výstupu
        " @return typ výstupu: stdout nebo jméno souboru, viz constants.py
        """
        return self._typeOfOutput
        
    def getOutputFile(self):
        """ 
        " Metoda vrátí jméno výstupního souboru.
        " @return jméno výtupního souboru.
        """
        return self._outputFile
        
    def getSeparator(self):
        """ 
        " Metoda vrátí oddělovač buněk v CSV
        " @return oddělovač buněk v CSV
        """
        return self._separator
        
    def isFirstRowHeader(self):
        """ 
        " Metoda vrátí příznak, který určuje, jestli se má první záznam CSV souboru považovat za hlavičku
        " @return true nebo false
        """
        return self._firstRowInCSVIsHeader
        
    def getSubstitutionForCSVHeader(self):
        """ 
        " Metoda vrátí náhradu za nevalidní znaky v názvu xml elementu
        " @return náhrada za nevalidní znaky v názvu xml elementu
        """
        return self._substitutionForCSVHeader

    def getPrefixOfColumnElement(self):
        """ 
        " Metoda vrací prefix pro název každého sloupce ve výstupním XML souboru, který se použije pokud nemá CSV soubor hlavičku
        " @return vrací prefix pro název každého nepojmenovaného sloupce ve výstupním XML souboru
        """
        return self._columnElement
     
    def getNameOfLineElement(self):
        """ 
        " Metoda vrací název pro XML elementy obalující řádky CSV 
        " @return název pro XML elementy obalující řádky CSV 
        """
        return self._lineElement    
        
    def isSetAttrIndex(self):
        """ 
        " Metoda vrátí příznak, který určuje, jestli se má řádkům v XML nastavit atribut index, zároveň vrátí poč. hodnotu tohoto atributu
        " @return True nebo False, poč. hodnota atributu index
        """
        return [self._setAttrIndex, self._startCounterForIndex]
        
    def doErrorRecovery(self):
        """ 
        " Metoda vrátí příznak, který určuje, jestli se má provádět zotavení z chyb CSV souboru
        " @return True nebo False
        """
        return self._errorRecovery
     
    def getMissingFieldValue(self):
        """ 
        " Metoda vrátí hodnotu, která se doplní do prázdných buněk CSV
        " @return hodnota, která se doplní do prázdných buněk CSV
        """
        return self._missingFieldValue    
        
    def includeAllColumn(self):
        """ 
        " Metoda vrátí příznak, který určuje, jestli sloupce, které jsou v nekorektním CSV navíc jsou ignorovány a nebo jsou zahrnuty do výsledku
        " @return True nebo False
        """
        return self._includeAllColumns
            
    def getRootElement(self):
        """ 
        " Metoda vrátí název kořenového XML elementu
        " @return název kořenového XML elementu nebo prázdný řetězec, pokud kořenový element se generovat nebude
        """
        return self._rootElement
        
    def generateXMLHeader(self):
        """ 
        " Metoda vrátí příznak, který určuje, jestli se bude generovat XML hlavička
        " @return True nebo False
        """
        return self._generateXMLHeader
