

#CSV:xherec00

""" 
" Kódování: UTF-8
" Autor: Jan Herec, xherec00@stud.fit.vutbr.cz
" Datum vytvoření: 16. 4. 2015
" Popis: Třída Parser
"""

from Const import *
import CSV
import sys
import Scanner

class Parser:
    """ Třída reprezentuje syntaktický analyzátor pro analýzu CSV souboru """
    
    def __init__(self, inputCSV, fieldSeparator, records):
        """ 
        " Metoda se zavolá po vytvoření objektu. Zároveň provede vytvoření a inicializaci atributů objektu.
        " @param inputCSV řetězec, který obsahuje načtený CSV soubor
        " @param fieldSeparator oddělovač buněk v CSV souboru
        " @param records pole záznamů, do kterého budeme ukládat jednotlivé záznamy, pole je implicitně předáno odkazem
        " @return void
        """
        
        self.__fieldSeparator = fieldSeparator                      # nastavíme oddělovač buněk v CSV souboru
        self.__scanner = Scanner.Scanner(inputCSV, fieldSeparator)  # vytvoříme a uložíme si instanci lexikálního analyzátoru
        self.__records = records                                    # provedem mělkou kopii pole předaného odkazem do atributu objektu

    
    def beginSyntacticAnalysis(self):
        """
        " Metoda začne provádět syntaktickou analýzu CSV souboru metodou rekurzivního sestupu
        " @return kód chyby ze třídy Const, určující jestli nastala při analýze chyba
        """

        return self.__nonTerminal_CSV_FILE()
    
    def __nonTerminal_CSV_FILE(self):
        """
        " Metoda simuluje analýzu neterminálu <CSV_FILE>
        " @return kód chyby ze třídy Const, určující jestli nastala při analýze chyba
        """
    
        ##### pravidlo: <CSV_FILE> -> <RECORD> <RECORD_N> $

        # ČÁST: <RECORD>
        errorCode = self.__nonTerminal_RECORD()

        if (errorCode != Const.ALL_OK):
            return errorCode;   

        # ČÁST: <RECORD_N>
        errorCode = self.__nonTerminal_RECORD_N()
        if (errorCode != Const.ALL_OK):
            return errorCode;  
            
        # ČÁST: $
        tokenType, tokenValue = self.__scanner.getNextToken()
        if (tokenType != Const.TOKEN_EOF):
            return Const.ERROR_BAD_INPUT_FILE_FORMAT;
        
        # vrátíme příznak toho, že při zpracování tohoto neterminálu proběhlo vše v pořádku
        return Const.ALL_OK
    
    def __nonTerminal_RECORD(self):
        """
        " Metoda simuluje analýzu neterminálu <RECORD>
        " @return kód chyby ze třídy Const, určující jestli nastala při analýze chyba
        """

        record = [] # vytvoříme záznam, který bude obsahovat jednotlivé buňky záznamu

        ##### pravidlo: <RECORD> -> <FIELD> <FIELD_N>
        
        # ČÁST: <FIELD>
        errorCode = self.__nonTerminal_FIELD(record)
        if (errorCode != Const.ALL_OK):
            return errorCode;  

        # ČÁST: <FIELD_N>
        errorCode = self.__nonTerminal_FIELD_N(record)
        if (errorCode != Const.ALL_OK):
            return errorCode;  

        self.__records.append(record) # uložíme do pole záznamů aktuálně načtený záznam
            
        # vrátíme příznak toho, že při zpracování tohoto neterminálu proběhlo vše v pořádku
        return Const.ALL_OK
        
    def __nonTerminal_RECORD_N(self):
        """
        " Metoda simuluje analýzu neterminálu <RECORD_N>
        " @return kód chyby ze třídy Const, určující jestli nastala při analýze chyba
        """

        ##### pravidlo: <RECORD_N> -> ε
        
        # ČÁST: epsilon
        tokenType, tokenValue = self.__scanner.getNextToken()
        if (tokenType == Const.TOKEN_EOF):
            
            # vrátíme token, který jsme přečetli, protože jej bude očekávat nadřazený neterminál
            self.__scanner.ungetToken([tokenType, tokenValue]);
            
            # vrátíme příznak toho, že při zpracování tohoto neterminálu proběhlo vše v pořádku
            return Const.ALL_OK;
        
        ##### pravidlo: <RECORD_N> -> crlf <RECORD> <RECORD_N>
        
        # ČÁST: crlf
        elif (tokenType == Const.TOKEN_CRLF):
            
            # ČÁST: <RECORD>
            errorCode = self.__nonTerminal_RECORD()
            if (errorCode != Const.ALL_OK):
                return errorCode;  
            
            # ČÁST: <RECORD_N>
            errorCode = self.__nonTerminal_RECORD_N()
            if (errorCode != Const.ALL_OK):
                return errorCode;  
                
            # vrátíme příznak toho, že při zpracování tohoto neterminálu proběhlo vše v pořádku
            return Const.ALL_OK
        
        # chyba
        else:
            return Const.ERROR_BAD_INPUT_FILE_FORMAT;

    
    def __nonTerminal_FIELD(self, record):
        """
        " Metoda simuluje analýzu neterminálu <FIELD>
        " @param record pole, které představuje záznam, do kterého budeme ukládat jednotlivé buňky, implicitně předán odkazem
        " @return kód chyby ze třídy Const, určující jestli nastala při analýze chyba
        """

        ##### pravidlo: <FIELD> -> epsilon 
        
        # ČÁST: epsilon
        tokenType, tokenValue = self.__scanner.getNextToken()
        if (tokenType == Const.TOKEN_EOF or tokenType == Const.TOKEN_CRLF or tokenType == Const.TOKEN_SEPARATOR):
            
            # vrátíme token, který jsme přečetli, protože jej bude očekávat nadřazený neterminál
            self.__scanner.ungetToken([tokenType, tokenValue]);
            
            record.append("") # uložíme do záznamu aktuálně načtenou buňku
            
            # vrátíme příznak toho, že při zpracování tohoto neterminálu proběhlo vše v pořádku
            return Const.ALL_OK;
            
        ##### pravidlo: <FIELD> -> field_content
        
        # ČÁST: field_content
        elif (tokenType == Const.TOKEN_FIELD):
            
            record.append(tokenValue) # uložíme do záznamu aktuálně načtenou buňku
            
            # vrátíme příznak toho, že při zpracování tohoto neterminálu proběhlo vše v pořádku
            return Const.ALL_OK;
        
        # chyba
        else:
            return Const.ERROR_BAD_INPUT_FILE_FORMAT;
    
    def __nonTerminal_FIELD_N(self, record):
        """
        " Metoda simuluje analýzu neterminálu <FIELD_N>
        " @param record pole, které představuje záznam, do kterého budeme ukládat jednotlivé buňky, implicitně předán odkazem
        " @return kód chyby ze třídy Const, určující jestli nastala při analýze chyba
        """

        ##### pravidlo: <FIELD_N> -> epsilon 
        
        # ČÁST: epsilon
        tokenType, tokenValue = self.__scanner.getNextToken()
        if (tokenType == Const.TOKEN_EOF or tokenType == Const.TOKEN_CRLF):
            
            # vrátíme token, který jsme přečetli, protože jej bude očekávat nadřazený neterminál
            self.__scanner.ungetToken([tokenType, tokenValue]);
            
            # vrátíme příznak toho, že při zpracování tohoto neterminálu proběhlo vše v pořádku
            return Const.ALL_OK;
            
        ##### pravidlo: <FIELD> -> field_separator <FIELD> <FIELD_N>
        
        # ČÁST: field_separator
        elif (tokenType == Const.TOKEN_SEPARATOR):
            
            # ČÁST: <FIELD>
            errorCode = self.__nonTerminal_FIELD(record)
            if (errorCode != Const.ALL_OK):
                return errorCode;  
            
            # ČÁST: <FIELD_N>
            errorCode = self.__nonTerminal_FIELD_N(record)
            if (errorCode != Const.ALL_OK):
                return errorCode;  
                
            # vrátíme příznak toho, že při zpracování tohoto neterminálu proběhlo vše v pořádku
            return Const.ALL_OK
        
        # chyba
        else:
            return Const.ERROR_BAD_INPUT_FILE_FORMAT;
    